# DS1302时钟传感器

## 效果展示

&emsp;&emsp;一个时钟传感器，可以设置时间，可以对年月日周时分秒进行计时，支持掉电保护（即在没有外部电源时也不会丢失时间数据），功能强大。本案例主要涉及的是其设置时间以及返回现在时间的功能。

&emsp;&emsp;下图为烧录后Waffle Nano显示屏上显示的数据。（最上面一行小字是实时的时间（初始化已预先做好，在使用时不要忘记初始化）接着是实时的年，月，日，星期几，时，分，秒）

![](image/20210720104637.jpg)

## 物理连接

### 传感器选择

&emsp;&emsp;传感器选择如下图所示的型号为DS1302的时钟传感器

![](image/20210720104612.jpg)

### 传感器接线

&emsp;&emsp;传感器与Waffle Nano之间的接线如下表所示（dat，clk，rst接线方法不唯一，比较自由，推荐用表中的接脚）

| Waffle Nano | 传感器 |
| ----------- | ------ |
| 3V3         | VCC    |
| GND         | GND    |
| G00         | CLK    |
| G02         | DAT    |
| G01         | RST    |

## 传感器库使用

&emsp;&emsp;可以[在此](/code/DS1302.py)获取库文件，将此库通过Waffle Nano的上传功能传到Waffle Nano开发板上。

&emsp;&emsp;可以通过如下代码来引用库

```python
import DS1302
```

&emsp;&emsp;引用完库之后，需要先将其中的class DS1302实例化

```python
ds=DS1302.DS1302(Pin(0),Pin(2),Pin(1))
```

&emsp;&emsp;接着就可以调用DS1302中的函数了，例如

```python
ds.DateTime() #调用DateTime函数，获取或者调整时间（年月日，星期几和时分秒）
```

```python
ds.Weekday()	#调用Weekday函数，设置或读取星期几
```