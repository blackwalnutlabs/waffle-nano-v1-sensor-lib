# ADPS9960手势传感器

## 传感器选择

&emsp;&emsp;传感器选择如下图中的APDS9960手势传感器

![](./image/20210720174312.jpg)

## 物理连接

### 传感器接线

&emsp;&emsp;传感器一共有六个接脚，其中LV不需要连接，INT为中断接脚，如果不需要终端功能的话也可以不连，下图是剩余四个接脚与Waffle Nano接脚的连线表

| Waffle Nano | ADPS9960 |
| ----------- | -------- |
| 3V3         | VCC      |
| GND         | GND      |
| G00         | SDA      |
| G01         | SCL      |

## 传感器库使用

&emsp;&emsp;[在此](./code/gesture_sensor.py)获取gesture_sensor.py（APDS9960库），将此库通过[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 的文件上传功能将此库上传到Waffle Nano上

&emsp;&emsp;使用方法为，先将库导入程序中

```python
import gesture_sensor
```

&emsp;&emsp;然后就可以直接用了（要注意先把屏幕初始化才能在屏幕上显示出方向）

```python
gesture=gesture_sensor.direction()
```


