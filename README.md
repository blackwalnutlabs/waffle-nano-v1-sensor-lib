# Waffle Nano 传感器库

&emsp;&emsp;此仓库描述各种传感器在Waffle Nano上的使用。

## 一般规范

* 文件夹名称为传感器型号英文名称
* 包含`code`文件夹，包含以传感器型号命名的传感器库文件
* 包含一份必要的说明文档，描述传感器接线，代码复现的方式

## 传感器目录

| 模块型号 | 模块名称 |
|---|---|
|[ATGM336H_5N](BDS_GPS/README.md)| 北斗、GPS模块 |
|[AMG8833](Thermography/GPIO/README.md)| 红外热成像 |
|[SGP30](AirQuality/README.md)| 空气质量传感器|
|[DS1302](Clock_DS1302/README.md)| 时钟传感器 |
|[DS1307](Clock_DS1307/README.md)| 时钟传感器 |
|[APDS9960](GestureSensor_APDS9960/README.md)| 手势传感器 |
|[paj7620](GestureSensor_paj7620/README.md)| 手势传感器 |
|[LM35](TemperatureSensor_LM35/README.md)| 温度传感器 |
|[AHT20](TemperatureSensor_AHT20/README.md)| 温度计 |
|[HDC1080](Humidity_TemperatureSensor_HDC1080/README.md)| 温湿度传感器 |
|[Buzzer](Buzzer/README.md)| 无源蜂鸣器 |
|[AD8232](ECGSensor_AD8232/README.md)| 心电图传感器 |
|[MAX_30102](OximetryHeart-rateSensor_MAX30102/README.md)| 心率血氧传感器模块 |
|[JoystickShield](JoystickShield/README.md)| 游戏摇杆扩展板 |

