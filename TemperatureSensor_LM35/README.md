# LM35温度传感器

## 物理连线

### 传感器选择

&emsp;&emsp;传感器选择如下图的型号为LM35的温度传感模块

<img src="img/IMG20210713193158.jpg" style="zoom:15%;" />

### 传感器连线

&emsp;&emsp;传感器与Waffle Nano 之间的接线方式如下表所示

| Waffle Nano | 传感器 |
| ---------- | ------ |
| 3V3         | VCC    |
| IO13        | OUT    |
| GND         | GND    |

## 传感器库使用

&emsp;&emsp;可以获取[LM35.py](./code/LM35.py)，将此库通过[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor)的文件上传到`Waffle Nano`上。

&emsp;&emsp;因为该传感器测温库较为简单，所以我们可以直接在主函数中使用以下代码导入此库

```python
import LM35
```

&emsp;&emsp;接着，定义一个新的变量接收`temp()`方法传回来的结果

```python
te=LM35.temp()#获取温度
```


