#导入库
from machine import UART,Pin
import utime

#定义GPS模组类
class ATGM336H_5N():
    def __init__(self,uart):
        self.msg=""
        self.sign=0
        self.sign2=0
        self.final=""
        #经度参数
        self.longitude=""
        #纬度参数
        self.latitude=""
        #南/北经标记
        self.latitude_direction=""
        #东/西经标记
        self.longitude_direction=""
        self.uart = uart

    def analysis(self):
        while(True):
            #如果uart缓冲区非空
            while (not self.uart.bufempty()):
                try:
                    temp=str(self.uart.read(1),'utf-8')
                except UnicodeError as e:
                    time.sleep_us(1)
                self.msg=self.msg+temp
                if(temp=="\n"):
                    if(self.msg[0]=="$"):
                        self.sign=1
                        break
                    else:
                        self.msg=""
            if(self.sign==1):
                self.sign=0
                if(self.msg[0:6]=="$GNRMC"):
                    count=0
                    self.sign2=1
                    while(self.msg.find(',')!=-1):
                        index=self.msg.find(',')
                        if(count==3):
                            self.latitude=self.msg[0:index]
                        if(count==4):
                            self.latitude_direction=self.msg[0:index]
                        if(count==5):
                            self.longitude=self.msg[0:index]
                        if(count==6):
                            self.longitude_direction=self.msg[0:index]
                        self.msg=self.msg[index+1:]
                        count=count+1
                if(self.sign2==1):
                    N=0
                    if(self.latitude_direction=="N"):
                        N=1
                    E=0
                    if(self.longitude_direction=="E"):
                        E=1
                    self.sign2=0
                self.msg=""
                #返回信息元组
                return(self.latitude,self.latitude_direction,self.longitude,self.longitude_direction)