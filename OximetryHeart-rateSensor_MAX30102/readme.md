# 心率血氧传感器模块

## 物理连接

### 传感器选择

![](img/waffle_nano_max30102.jpg)

&emsp;&emsp;传感器选择如上图所示的型号为MAX30102的心率血氧传感器模块。

### 传感器接线

&emsp;&emsp;传感器与Waffle Nano 之间的接线方式如下表所示，且未在下表中显示的引脚均处于悬空不连装态。

| Waffle Nano |      |
| ----------- | ---- |
| 3.3         | UIN  |
| IO0         | SDA  |
| IO1         | SCL  |
| GND         | GND  |

## 传感器库使用

&emsp;&emsp;可以获取[max30102.py](./code/max30102.py),将此库通过[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 的文件上传功能将此库上传到`Waffle Nano`上。

&emsp;&emsp;我们在可以在主函数中使用以下代码导入此库。

```
import max30102
```

&emsp;&emsp;在对象构造函数中，我们需要传入一个已经构造好的`IIC`对象,并要给出sda,scl,采样频率的数值。

```
# 此处省略IIC对象的构造过程
m = max30102.Max30102(0,1,100) #构造max30102对象
```

&emsp;&emsp;我们使用实时时钟对象的read_fifo()方法读取出red和ir的数值。

```
red,ir = m.read_fifo() #从血氧芯片中获取数据
```

&emsp;&emsp;关于此库相关细节说明详见代码注释