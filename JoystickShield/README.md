# Joystick Shield游戏摇杆扩展板

## 物理连接

### 传感器选择

&emsp;&emsp;本文所指的传感器为如下图所示的型号为Joystick Shield的游戏摇杆扩展板模组。

![](./image/实物图.jpg)

&emsp;&emsp;Joystick Shield的游戏摇杆扩展板的电源有3.3V和5V两个选项和切换3.3V和5V的开关，我们需要将其切换到3.3v状态。下文如果提到开关，一律见此处说明。



### 传感器接线

&emsp;&emsp;传感器与Waffle Nano 之间的接线方式如下表所示，且未在下表中显示的引脚均处于悬空不连装态。

| Waffle Nano | 传感器 |
| ----------- | ------ |
| 3.3V        | 3.3V   |
| IO14        | A      |
| IO0         | B      |
| IO1         | C      |
| IO2         | D      |
| IO12        | X      |
| IO13        | Y      |
| GND         | GND    |

### 传感器接线示意图

&emsp;&emsp;注：传感器引脚上并无标签，如果尝试连线，请参照下图或咨询卖家。

![](https://gitee.com/fancifulnarrator/waffle-nano-v1-sensor-lib/raw/master/%20JoystickShield/image/%E5%BC%95%E8%84%9A%E5%9B%BE%EF%BC%88%E5%AE%9E%E7%89%A9%EF%BC%89.jpg)



## 传感器库使用

&emsp;&emsp;该传感器在WaFFle中的库为[joystickshield.py](./code/joystickshield.py),下载此库，并通过[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 的文件上传功能将此库上传到`Waffle Nano`板子上，即可引用。

&emsp;&emsp;引用该库的代码为

```python
from joystickshield import JoystickShield
```

&emsp;&emsp;想要实例化一个`JoystickShield`对象`joystick`，需要4个GPIO引脚，两个ADC引脚。构造方法如下:

```python
# 引脚引用过程省略

joystick = JoystickShield(pinA, pinB, pinC, pinD, adcX, adcY)
```

&emsp;&emsp;`joystick`对象的`read()`方法返回的是一个有六个元素的数组。分别代表意思详见库文件中的注释。


```python
Nowdata = joystick.read()
```
