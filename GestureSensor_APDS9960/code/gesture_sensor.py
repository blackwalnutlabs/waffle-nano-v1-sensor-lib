def direction():
    from machine import I2C, Pin
    import utime,math
    i2c = I2C(1, sda=Pin(0), scl=Pin(1), freq=115200)
    #print(i2c.scan())
    #print(i2c.readfrom_mem(57,0x92,1))
    i2c.write(57,b'\x80\x45')                   #传感器的地址为0x39，换算成十进制为57，向传感器中地址为0x80的控制功能开关寄存器中写入0x45，即开启电源，开启手势识别和接近识别
    #print(i2c.readfrom_mem(57,0x80,1))
    i2c.write(57,b'\xaf\x01')                   #向传感器中地址为0xaf的判断手势是否有效的寄存器内写入0x01，即有效
    #print(i2c.readfrom_mem(57,0xaf,1))         #带“#”号的可以帮助检测传感器是否正常运作    
    #print(i2c.readfrom_mem(57,0xae,1))
    container=[]
    temp1=0
    temp2=0
    left_right_diff=0
    up_down_diff=0
    count=0
    while(1):                               #一直获取数据
        gesture=''
        container=i2c.readfrom_mem(57,0xfc,4)       #获取从地址为0xfc往后4个寄存器的信息（位置信息，包括地址为0xfc的寄存器）
        utime.sleep_ms(30)
        if (math.fabs(int(container[0])-int(container[1])) > 50):
            up_down_diff = int(container[0]) - int(container[1])
        if (math.fabs(int(container[2])-int(container[3])) > 13):
            left_right_diff = int(container[2]) - int(container[3])
        if not up_down_diff == 0:
            if (up_down_diff > 0):
                if (temp1 > up_down_diff):
                    #print("↑")
                    gesture='up'
                elif (temp1 <up_down_diff):
                    #print("↓")
                    gesture='down'
        if not left_right_diff == 0:
            if (left_right_diff > 0):
                if (temp2 > left_right_diff):
                    #print("←")
                    gesture='left'
                elif (temp2< left_right_diff):
                    #print("→")
                    gesture='right'
        if(up_down_diff!=0):            
            temp1=up_down_diff
        if(left_right_diff!=0):
            temp2=left_right_diff
        count+=1
        #print(count,up_down_diff,left_right_diff)
        if(count%2==0):
            utime.sleep_ms(400)
            count=0
            return gesture 