### PAJ7620U2手势传感器

## 物理连接

### 传感器选择

&emsp;&emsp;传感器选择如下图所示的型号为PAJ7620U2的手势识别传感器模组

![7620](https://gitee.com/xinyuan-zhu/waffle-nano-v1-sensor-lib/raw/master/paj7620_D/img/7620.jpg)

### 传感器接线

&emsp;&emsp;传感器与waffle Nano之间的接线方式如下表所示，且未在下表中显示的引脚均处于悬空不连状态。

| Waffle Nano | 传感器 |
| ----------- | ------ |
| 3.3         | VIN    |
| IO1         | SCL    |
| IO0         | SDA    |
| GND         | GND    |

## 传感器库使用

&emsp;&emsp;可以获取[paj7620u2.py](./code/paj7620.py),将此库通过[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 的文件上传功能将此库上传到`Waffle Nano`上。

&emsp;&emsp;我们在可以在主函数中使用以下代码导入此库。

```
from paj7620 import PAJ7620
```

&emsp;&emsp;在对象构造函数中，我们需要传入一个已经构造好的`IIC`对象

```python
from machine import I2C  

i2c = I2C(1, sda=Pin(0), scl=Pin(1), freq=400000)  

sensor7620=PAJ7620(i2c)
```

###  函数

&emsp;&emsp;下面列举传感器库中所使用的函数

####  寄存器初始化

```
__init__(self, i2c, addr)
```

&emsp;&emsp;函数说明：将寄存器初始化，传入i2c和需要通信的从机地址

#### 设置手势识别

```
set_gesture(self, reg, data)
```

&emsp;&emsp;函数说明：设置开启哪些手势可识别,  reg为手势识别寄存器序号,data为需要发送的数据(十六进制数), 范围[0,2^8 - 1]

####  获取手势数据

```
get_gesture(self,reg)
```

&emsp;&emsp;函数说明：返回寄存器值，不同值对应不同手势，reg为手势识别寄存器序号

&emsp;&emsp;返回值：

    reg==1:
        向上->[1]
        向下->[2]
        向左->[4]
        向右->[8]
        向前->[16]
        向后->[32]
        顺时针->[64]
        逆时针->[128]
    reg==2:
        挥动->[1]


