# 空气质量传感器

## 物理连接

### 传感器选择

&emsp; &emsp;传感器选择如下图所示的型号为SGP30的气体传感器模块。

![](./img/chuanganqi.jpg)

### 传感器接线

&emsp; &emsp;传感器与Waffle Nano之间的接线方式如下表所示。
![](./img/jiexian.png)

## 传感器库使用

&emsp; &emsp;可以获取sgp30.py将此库通过Waffle Maker的文件上传功能将此库上传到Waffle Nano上。

&emsp; &emsp;我们在可以在主函数中使用以下代码导入此库。

```python
from sgp30 import SGP30
```

&emsp; &emsp;我们使用空气质量测量仪对象的read（）方法读取出数据

```python

yq=SGP30(i2c) #构造SGP30对象

print(yp.read()) #读取传感器数据

```


&emsp; &emsp;关于此库相关细节说明详见代码注释



