# 北斗GPS模块

##  效果展示

&emsp;&emsp;如果在室内没有接受到信号时:

<img src="img/20210722095440.jpg" alt="20210722095440" style="zoom:50%;" />

&emsp;&emsp;在室外接收到信号时:

<img src="img/20210722095432.jpg" alt="20210722095432" style="zoom:50%;" />



## 物理连接

### 传感器选择 

<img src="img/20210721225022.jpg" style="zoom: 50%;" />

&emsp;&emsp;传感器选择如下图所示的型号为ATGM336H_5N的GPS模组。

### 传感器接线 

&emsp;&emsp;传感器与Waffle Nano 之间的接线方式如下表所示，且未在下表中显示的引脚均处于悬空不连装态。

| Waffle Nano |      |
| ----------- | ---- |
| 3.3         | VCC  |
| RX          | TX   |
| TX          | RX   |
| GND         | GND  |

## 传感器库使用

&emsp;&emsp;可以获取[ATGM336H_5N.py](./code/ATGM336H_5N.py),将此库通过[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 的文件上传功能将此库上传到`Waffle Nano`上。

&emsp;&emsp;我们在可以在主函数中使用以下代码导入此库。

```
 import ATGM336H_5N
```

&emsp;&emsp;在对象构造函数中，我们需要传入一个已经构造好的`UART`对象。

```
# 此处省略UART对象的构造过程
ag = ATGM336H_5N.ATGM336H_5N(uart) #构造GPS对象
```

&emsp;&emsp;我们使用GPS对象的analysis()方法读取解析出来的信息元组

```
Tmp = ag.analysis() #解析数据,获得信息元组
```

&emsp;&emsp;信息元组组成如下:(纬度参数, 纬度方向(N/S), 经度参数, 经度方向(E/W))

&emsp;&emsp;关于此库相关细节说明详见代码注释
