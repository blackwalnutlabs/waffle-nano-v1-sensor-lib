# DS1307 

## 传感器

&emsp;&emsp;DS1307使用I2C协议。

&emsp;&emsp;各寄存器地址如下：

![Timekeeper_Registers](./img/Timekeeper_Registers.png)


## 物理连线

&emsp;&emsp;Tiny RTC DS1307 与Waffle nano的连线选择左侧引脚较少一侧，只需与主板连四根线即可。

* SCL --> G01
* SDA --> G00
* VCC --> 3V3
* GND --> GND

## 传感器库

&emsp;&emsp;传感器库名为ds1307, 提供：

- 查询时间 get_time()
- 设置时间 set_time()
- 查询状态 query_halt()
- 设置状态 set_halt()

使用方法:

```python
from machine import I2C, Pin
import ds1307

i2c = I2C(1, sda = Pin(0), scl = Pin(1), freq = 100000)
ds = ds1307.DS1307(i2c) # 传入接口
ds.set_halt(False) # 启动计时
now = (year, month, day, date, hour, minute, second, subsecond) # 写入当前时间
ds.set_time(now) #设置时间
print(ds.get_time()) # 查询时间
```

## 代码样例：

```python
>>> from machine import I2C, Pin
>>> import ds1307
>>> i2c = I2C(1, sda = Pin(0), scl = Pin(1), freq = 100000)
>>> ds = ds1307.DS1307(i2c)
>>> i2c.scan()					# 查询接口地址
[80, 104]
>>> ds.query_halt() 			# 查询时钟是否暂停
False							# 时钟未暂停
>>> ds.get_time() 				# 查询时间
(2021, 7, 16, 5, 20, 54, 7, 0)  # 当前时间
>>> ds.get_time() 				# 等待 8s
(2021, 7, 16, 5, 20, 54, 16, 0) 
>>> ds.get_time()				# 等待 10s
(2021, 7, 16, 5, 20, 54, 26, 0)
>>> ds.set_halt(True)			# 设置时钟暂停计时
>>> ds.get_time()				# 获取时间
(2021, 7, 16, 5, 20, 54, 43, 0) 
>>> ds.get_time()				# 等待 5s
(2021, 7, 16, 5, 20, 54, 43, 0) # 时间未变
>>> ds.set_halt(False)			# 设置时钟开启计时
>>> ds.get_time()				# 获取时间
(2021, 7, 16, 5, 20, 54, 49, 0)
>>> ds.get_time()				#等待 5S
(2021, 7, 16, 5, 20, 54, 54, 0)
```

