# AHT20温度计

&emsp;&emsp;用于AHT20温度和湿度传感器的MicroPython驱动程序。(AHT10也适用)

# 传感器选择

<img src="image/2.jpg" style="zoom:50%;" />

<img src="image/1.jpg" style="zoom: 80%;" />


# 物理连线

| Waffle Nano | 传感器  |
| ----------- | ------ |
| G01         | SCL   |
| G00         | SDA    |
| 3V3         | VCC    |
| GND         | GND    |


## 传感器库使用
```python
import ahtx0
```

可以获取[ahtx0.py](https://gitee.com/blackwalnutlabs/waffle-nano-v1-sensor-lib/blob/master/AHT20/code/ahtx0.py)，将此库通过[Waffle Maker](https://wafflenano.lwb2892844157.tech/ide/index.html#/editor)的文件上传到`Waffle Nano`上。

关于此库相关细节说明详见代码注释。
## 样例代码

```python
import utime
from machine import Pin, I2C

import ahtx0
i2c = I2C(1, sda=Pin(0), scl=Pin(1), freq=100000)

# 使用I2C创建传感器对象
sensor = ahtx0.AHT20(i2c)

while True:
    print("\nTemperature: %0.2f C" % sensor.temperature)
    print("Humidity: %0.2f %%" % sensor.relative_humidity)
    utime.sleep(5)
```