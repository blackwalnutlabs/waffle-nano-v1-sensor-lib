# 数字简谱

## 案例展示

&emsp;&emsp;基于十二平均律，将音乐简谱中的 `音高、拍号、拍速、减增时线、升降符、变化音和单音` 转换为传递给无源蜂鸣器的 `PWM` 频率和播放时长，并控制蜂鸣器将其播放出来。

![](assets/image/Snipaste_2021-07-16_17-27-01.png)

> 注：由于芯片 `PWM` 频率限制，建议将传入的音高提高 `2` 个八度。

## 物理连接

### 传感器选择

&emsp;&emsp;传感器选择如下图所示的无源蜂鸣器。

![](assets/image/Snipaste_2021-07-16_17-32-24.png))

### 传感器接线

&emsp;&emsp;传感器与 `Waffle Nano` 之间的接线方式如下表所示，且未在下表中显示的引脚均处于悬空不连装态。

| Waffle Nano | 传感器       |
| ----------- | ------------ |
| IO7         | 任意一根引脚 |
| GND         | 另一根引脚   |


## 传感器库使用

&emsp;&emsp;可以获取 [NumberedMusicalNotation.py](assets/code/NumberedMusicalNotation.py)，并将此库通过 [Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 的文件上传功能将此库上传到 `Waffle Nano` 上。

&emsp;&emsp;我们在可以在主函数中使用以下代码导入此库。

``` python
from NumberedMusicalNotation import NumberedMusicalNotation 
```

&emsp;&emsp;在对象构造函数中，我们需要传入一个已经构造好的 `PWM` 对象。

``` python
# 此处省略 PWM 对象的构造过程
nmn = NumberedMusicalNotation(pwm) #构造  对象
```

---

&emsp;&emsp;`nmn.setSpeed` 方法，表示设置拍速。该方法需要传入 `1` 个参数，即对应简谱中的 `音乐符号=xxx`。以 `137` 为例。如图所示：

![](assets/image/Snipaste_2021-07-16_17-42-02.png)

``` python
nmn.setSpeed(137)
```

---

&emsp;&emsp;`nmn.setTimeSignature` 方法，表示设置拍号。该方法需要传入 `1` 个参数，即对应简谱上左上角的 `符号 x/y`，在设置时该值只和 `y` 有关。以 `4/4` 为例，如图所示：

![](assets/image/Snipaste_2021-07-16_17-47-31.png)

``` python
nmn.setTimeSignature(4)
```

---

&emsp;&emsp;`nmn.setPreAdd` 方法，表示整体基础音高。有些控制电路不支持向蜂鸣器传递较小的频率，因此为了让简谱中所有的音都能正常播放，并且不破坏音律的节奏，我们可以对简谱中每个音都进行整体提升音高。该方法需要传入 `1` 个参数，即需要对整个简谱升高或降低的八度数量，例如：

``` python
nmn.setPreAdd(2)
```

> 注：如控制芯片为 `Hi3861` 建议设置 `setPreAdd` 参数为 `2`。

---

&emsp;&emsp;`nmn.setMajor` 方法，表示设置初始调。该方法需要传入 `1` 个参数，即对应简谱中的 `1=xxx`。目前该库支持以下首调：

* C
* #C
* bD
* D
* #D
* bE
* E
* F
* #F
* bG
* G
* #G
* bA
* A
* #A
* bB
* B

``` python
# 1=C
nmn.setMajor('C')
```

---

---

&emsp;&emsp;`nmn.playNotation` 方法，表示控制蜂鸣器播放音乐。该方法需要传入 `1` 个参数，即对应简谱上的包含增减时符的单音字符。以 `Do` 为例：

``` python
nmn.playNotation('1')
```

&emsp;&emsp;符号对应如下：

* `b数字`：音高降半音
* `+`：音高高八度
* `-`：音高降八度
* `*`（附点）：时值延长前一个音的一半
* `.`（增时线）：时值延长一个四分音符（半音符、全音符等）
* `_`（减时线）：时值减半（八分音符、十六分音符等）
* `~`（上波音）

&emsp;&emsp;以上符号可以根据简谱内容随意组合，例如：

``` python
musicList = [
    '--6', '-3', '-4_', '-3__', '-2__', '-3',
]


for monophonic in musicList:
    nmn.playNotation(monophonic)
```

---

## 特别致谢

&emsp;&emsp;感谢 `FF14` 延夏新东方演奏群 的小伙伴们在简谱和乐理方面提供知识支持（以下排名不分先后）：

* Clavy
* 大酱dz
* 番茄沙司
* 年轻的八云紫
* 小黑鱼
* 原Via
* 宴月